# Project-01-Aloha
<h1>Aloha Apparel Website</h1>

<strong>Project 1 in the Web Development Professional course at RED Academy.</strong>

<p>The goal of the project was to learn HTML, CSS and get started in JavaScript and JQuery.</p>

<h6>Technologies used in this project:</h6>
<ul>
<li>HTML5</li>
<li>CSS3</li>
<li>JavaScript</li>
<li>JQuery 3.2.1</li>
<li>Autoprefixer</li>
</ul>

<h4>Screenshot:</h4>

![](images/aloha-readme-desktop.jpg)

![](images/aloha-readme-mobile.png)
